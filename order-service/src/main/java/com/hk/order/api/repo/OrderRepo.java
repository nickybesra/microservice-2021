package com.hk.order.api.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hk.order.api.entity.Order;

public interface OrderRepo extends JpaRepository<Order, Integer> {

}
