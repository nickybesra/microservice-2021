package com.hk.order.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hk.order.api.dto.TransactionRequest;
import com.hk.order.api.dto.TransactionResponse;
import com.hk.order.api.service.OrderService;

@RestController
@RequestMapping("/order")
public class OrderController {
	
	@Autowired
	private OrderService orderService;
	
	@PostMapping(value = "/bookOrder")
	public TransactionResponse bookOrder(@RequestBody TransactionRequest request) {
		TransactionResponse response = orderService.saveOrder(request);
		return response;
	}
	
	@GetMapping("/test")
	public String bookOrder() {
		return "test";
	}

}
