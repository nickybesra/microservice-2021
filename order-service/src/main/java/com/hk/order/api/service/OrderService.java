package com.hk.order.api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.hk.order.api.dto.Payment;
import com.hk.order.api.dto.TransactionRequest;
import com.hk.order.api.dto.TransactionResponse;
import com.hk.order.api.entity.Order;
import com.hk.order.api.repo.OrderRepo;

@Service
@RefreshScope
public class OrderService {

	@Autowired
	private OrderRepo orderRepo;

	@Autowired
	@Lazy
	private RestTemplate restTemplate;

	@Value("${payment.endpoints}")
	private String DO_PAYMENT_URL;

	public TransactionResponse saveOrder(TransactionRequest request) {
		System.err.println(request);
		TransactionResponse response = new TransactionResponse();
		Order order = request.getOrder();
		Payment payment = request.getPayment();
		payment.setOrderId(order.getId());
		response.setOrder(orderRepo.save(order));
		// rest call
		Payment paymentResponse =
				// restTemplate.postForObject("http://localhost:8083/payment/doPayment",payment,
				// Payment.class);
				// restTemplate.postForObject("http://PAYMENT-SERVICE/payment/doPayment",payment,
				// Payment.class);
				restTemplate.postForObject(DO_PAYMENT_URL + "/doPayment", payment, Payment.class);
		response.setMessage(
				paymentResponse.getStatus().equalsIgnoreCase("success") ? "payment successful" : "payment failed");
		response.setTransactionId(paymentResponse.getTransactionId());
		return response;
	}
}
