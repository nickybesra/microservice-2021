package com.hk.payment.api.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hk.payment.api.entity.Payment;

public interface PaymentRepo extends JpaRepository<Payment, Integer> {

}
