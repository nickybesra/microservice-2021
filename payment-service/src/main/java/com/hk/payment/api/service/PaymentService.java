package com.hk.payment.api.service;

import java.util.Random;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hk.payment.api.entity.Payment;
import com.hk.payment.api.repo.PaymentRepo;

@Service
public class PaymentService {
	
	@Autowired
	private PaymentRepo paymentRepo;
	
	public Payment doPayment(Payment payment) {
		payment.setTransactionId(UUID.randomUUID().toString());
		payment.setStatus(paymentStatus());
		return paymentRepo.save(payment);
	}
	
	public String paymentStatus() {
		return new Random().nextBoolean()?"success":"Failure";
	}

}
