package com.hk;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HkApiGatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(HkApiGatewayApplication.class, args);
	}

}
